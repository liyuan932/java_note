** 常用插件
File
⌘+, 全局设置
⌘+; 项目设置
⌘+Q Exit

Edit
⌘+F  查找 
⌘+R 替换
CTRL+SHIFT+F  全局查找
CTRL+SHIFT+F  全局替换
⌘+D   复制行
⌘+Y   删除行
⌘+SHIFT+U 大小写开关
ALT+SHIFT+J  合并行
SHIFT+F6 重命名文件



View
CTRL+J  文档
⌘+P 方法参数信息
⌘+D 文件对比（无光标状态）
⌘+CTRL+F  全屏切换
Navigate 
双击Shift 查找任何东西
⌘+N   查找类
⌘+SHIFT+N 查找文件
⌘+G 查找行
⌘+ALT+左右箭头  前进/后退
⌘+B  查找变量、类、方法的声明
⌘+ALT+B  查看实现
⌘+U 查看父类
⌘+F12 查看文件结构
CTRL+H 查看类继承关系
CTRL+ALT+H 查看方法调用关系
F2  定位错误 
CTRL+1 显示/隐藏项目视图



Code
CTRL+N 或  CTRL+ENTER 代码生成
ALT+/ 自动提示
⌘+ALT+L 格式化代码
⌘+ALT+O 优化包导入
ALT+ENTER 自动纠错
⌘+/  单行注释
CTRL+SHIFT+/  多行注释

CVS
⌘+T Update Project
⌘+K  Commit Changes
⌘+Shift+K Push Commits
⌘+Alt+A   Add
⌘+Alt+Z  Revert

查找文件和文本
双击Shift  查找任何东西
⌘+F  Find...
⌘+R Replace...
CTRL+SHIFT+F  Find in Path...
CTRL+SHIFT+F  Replace in Path...
⌘+D   复制行
⌘+Y   删除行




