## JSON-RPC接口验证API

## 请求格式
{"jsonrpc": "2.0", "method": "getUser", params: [1,2], id: 1234}

### validator包依赖问题
spring-boot-starter-web-1.5.9.RELEASE依赖的validator API为1.1.0.Final
``` xml
<javax-validation.version>1.1.0.Final</javax-validation.version>
<hibernate-validator.version>5.3.6.Final</hibernate-validator.version>
```
修改为2.0.1.Final
``` xml
<validation-api.version>2.0.1.Final</validation-api.version>
<hibernate-validator.version>6.0.7.Final</hibernate-validator.version>
```
hibernate-validator的Maven Group被迁移到org.hibernate.validator，在Spring boot尽量不要使用新的依赖，不然的话就要排包
