## spring-boot-maven-plugin插件
### 引用
* http://docs.spring.io/spring-boot/docs/current/maven-plugin/
* http://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html

### 插件目标
* `spring-boot:run` 启动spring boot
* `spring-boot:repackage` 生成一个可执行的jar或war包，默认绑定在package阶段
* `srping-boot:build-info`  生成构建信息

### 插件配置
``` xml
<configuration>
    <fork>true</fork>
    <executable>false</executable>
    <excludeDevtools>true</excludeDevtools>
</configuration>
```

### 有多个main方法时，如何指定用哪个启动spring boot
https://stackoverflow.com/questions/23217002/how-do-i-tell-spring-boot-which-main-class-to-use-for-the-executable-jar


## CommandLineRunner和ApplicationRunner
> spring boot应用启动时执行，可用来加载配置文件、清除缓存等初始化工作。多个实例可使用@Order指定执行顺序。
`两者区别:`ApplicationRunner可以更详细地获取命令行参数。

### 引用 
http://blog.csdn.net/gebitan505/article/details/55047819

### 示例
``` java
@Bean
CommandLineRunner sampleCommandLineRunner() {
    return (args) -> System.out.println(this.cityMapper.findByState("CA"));
}
```

## @SpringBootApplication注解
```
等价于 
@EnableAutoConfiguration
@ComponentScan
@Configuration
```

## spring boot热部署
http://blog.csdn.net/zknxx/article/details/53428669