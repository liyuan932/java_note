### 引用
* http://blog.csdn.net/zknxx/article/details/53428669
* https://www.cnblogs.com/magicalSam/p/7196355.html


### 配置
``` xml
<dependency>  
    <groupId>org.springframework.boot</groupId>  
    <artifactId>spring-boot-devtools</artifactId>  
    <optional>true</optional> 
</dependency>  
```

### 监听文件夹的变化
```
spring.devtools.restart.additional-paths=com\\zkn\\learnspringboot
``` 

### 页面热部署
```
spring.thymeleaf.cache=false
```